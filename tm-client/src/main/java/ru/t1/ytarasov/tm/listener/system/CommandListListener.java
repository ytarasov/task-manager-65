package ru.t1.ytarasov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.model.IListener;
import ru.t1.ytarasov.tm.event.ConsoleEvent;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "command";

    @Nullable
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list";

    @Override
    @EventListener(condition = "@commandListListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[COMMANDS]");
        for (@NotNull final IListener listener : listeners) {
            @Nullable final String name = listener.getName();
            System.out.println(name);
        }
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
