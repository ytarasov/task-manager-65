package ru.t1.ytarasov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
