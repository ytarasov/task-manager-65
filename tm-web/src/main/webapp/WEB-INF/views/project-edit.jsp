<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>EDIT PROJECT</h1>

<div id="formEdit">
	<form:form action="/project/edit/${project.id}" method="POST" modelAttribute="project">
	    <form:input type="hidden" name="id" path="id" />
		<p>NAME: </p><form:input type="text" name="name" path="name" />
		<p>DESCRIPTION: </p><form:input type="text" name="description" path="description" />
		<p>DATE START: </p><form:input type="date" path="dateStart" />
		<p>DATE FINISH: </p><form:input type="date" path="dateFinish" />
		<p>STATUS</p>
		<form:select path="status">
		    <form:options items="${statuses}" itemLabel="displayName" />
		</form:select>
		<br>
		<br>
		<button type="submit">SAVE</button>
	</form:form>
</div>

<jsp:include page="../include/_footer.jsp"/>