package ru.t1.ytarasov.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.ytarasov.tm")
public class ApplicationConfiguration {
}
