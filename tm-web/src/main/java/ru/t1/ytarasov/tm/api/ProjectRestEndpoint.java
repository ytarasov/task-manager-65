package ru.t1.ytarasov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.model.Project;

import java.util.Collection;
import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @PutMapping("/create")
    void create();

    @PostMapping("/save")
    void save(@RequestBody Project project);

    @GetMapping("/findAll")
    Collection<Project> findAll();

    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody Project project);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<Project> projects);

    @DeleteMapping("/clear")
    void clear();

}
