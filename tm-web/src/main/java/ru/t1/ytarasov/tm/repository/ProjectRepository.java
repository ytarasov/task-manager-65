package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectRepository {

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        for (int i = 1; i < 4; i++) {
            save(new Project("TEST PROJECT " + i, "TEST PROJECT"));
        }
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        save(new Project("New project " + System.currentTimeMillis()));
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void deleteById(@NotNull final String id) {
        projects.remove(id);
    }

    public long count() {
        return projects.size();
    }

    public boolean existsById(@NotNull final String id) {
        return projects.containsKey(id);
    }

    public void delete(Project project) {
        projects.remove(project.getId());
    }

    public void deleteAll(List<Project> projectList) {
        projectList.forEach(this::delete);
    }

    public void clear() {
        projects.clear();
    }

}
