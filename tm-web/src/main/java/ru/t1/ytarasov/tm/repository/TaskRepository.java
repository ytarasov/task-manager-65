package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        for (int i = 1; i < 4; i++) save(new Task("TEST TASK" + i));
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        save(new Task("New task " + System.currentTimeMillis()));
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public long count() {
        return tasks.size();
    }

    public boolean existsById(@NotNull final String id) {
        return tasks.containsKey(id);
    }

    public void deleteById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void delete(Task task) {
        tasks.remove(task.getId());
    }

    public void deleteAll(List<Task> taskList) {
        taskList.forEach(this::delete);
    }

    public void clear() {
        tasks.clear();
    }

}
