package ru.t1.ytarasov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.repository.ProjectRepository;

import java.util.Collection;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectRepository projectRepository;

    public Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("projects", "projects", getProjects());
    }

}
