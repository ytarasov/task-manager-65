package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.ProjectRestEndpoint;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @PutMapping("/create")
    public void create() {
        projectRepository.create();
    }

    @Override
    @PostMapping("/save")
    public void save(@NotNull @RequestBody final Project project) {
        projectRepository.save(project);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.findById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectRepository.count();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectRepository.delete(project);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<Project> projects) {
        projectRepository.deleteAll(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectRepository.clear();
    }

}
