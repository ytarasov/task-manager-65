package ru.t1.ytarasov.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ytarasov.tm.component.Bootstrap;
import ru.t1.ytarasov.tm.configuration.LoggerConfiguration;
import ru.t1.ytarasov.tm.listener.EntityListener;
import ru.t1.ytarasov.tm.service.LoggerService;

import javax.jms.*;

public class ConsumerApplication {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
