package ru.t1.ytarasov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractModel {

    public User(@NotNull String login, @Nullable String passwordHash, @NotNull String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @NotNull
    @Column(nullable = false)
    private String login;

    @Nullable
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @NotNull
    @Column(nullable = false)
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Column(name = "locked")
    private boolean isLocked = false;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Task> tasks;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Project> projects;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Session> sessions;

}
